import inquirer from "inquirer";

import genKeypair from "./tools/genKeypair.js";
import sign from "./tools/sign.js";

(async () => {
    console.log("jottocraft JWT tool\n");

    while (true) {
        console.log("\n");

        const answers = await inquirer.prompt([
            {
                type: "list",
                name: "tool",
                message: "Select a tool",
                choices: [
                    "Generate a keypair",
                    "Sign a JWT",
                    "Exit"
                ]
            }
        ]);

        console.log("\n");

        if (answers.tool === "Generate a keypair") {
            await genKeypair();
        } else if (answers.tool === "Sign a JWT") {
            await sign();
        } else if (answers.tool === "Exit") {
            console.log("\nGoodbye");
            break;
        }
    }
})();