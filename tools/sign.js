import { SignJWT, importJWK } from "jose";
import fs from "fs/promises";
import path from "path";
import { fileURLToPath } from "url";

export default async function sign() {
    console.log("Signing jwt-data.json -> jwt-signed.txt...");

    const __dirname = path.dirname(fileURLToPath(import.meta.url));

    const privateJwkJSON = await fs.readFile(path.join(__dirname, "..", "private-jwk.json"));
    const privateJWK = JSON.parse(String(privateJwkJSON));
    const privateKey = await importJWK(privateJWK);

    const jwtData = await fs.readFile(path.join(__dirname, "..", "jwt-data.json"));

    const jwtSigned = await new SignJWT(JSON.parse(String(jwtData)))
        .setProtectedHeader({ alg: "RS256" })
        .setIssuedAt()
        .sign(privateKey);

    await fs.writeFile(path.join(__dirname, "..", "jwt-signed.txt"), jwtSigned);

    console.log("Done!");
}