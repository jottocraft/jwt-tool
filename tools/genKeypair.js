import { generateKeyPair, exportJWK } from "jose";
import fs from "fs/promises";
import path from "path";
import { fileURLToPath } from "url";

export default async function genKeypair() {
    console.log("Generating a public/private RS256 keypair...");

    const pair = await generateKeyPair("RS256", {
        extractable: true
    });

    const __dirname = path.dirname(fileURLToPath(import.meta.url));
    await fs.writeFile(path.join(__dirname, "..", "private-jwk.json"), JSON.stringify(await exportJWK(pair.privateKey)));
    await fs.writeFile(path.join(__dirname, "..", "public-jwk.json"), JSON.stringify(await exportJWK(pair.publicKey)));

    console.log("Done!");
}